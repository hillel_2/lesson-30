import re

pattern = re.compile('[a-z]([A-Z])')
test_phrase = 'Hello World_9 and HeLlo. Welcome to our hoTel "Hillel"'
test_phrase2 = 'Hello, I`m Sasha, I`m 33 years old. Born_at 32 NoShaveNovember.'

print(pattern.findall(test_phrase))
print(re.findall(r'\d', test_phrase2))
print(re.findall(r'\d+', test_phrase2))
print(re.findall(r'\D+', test_phrase2))
print(re.findall(r'\D', test_phrase2))
print(re.findall(r'.', test_phrase2))
print(re.findall(r'.+', test_phrase2))
print(re.findall(r'\w+', test_phrase2))
print(re.findall(r'\d+\s+\w+\.', test_phrase2))
print(re.findall(r'\d+\s+\w+.', test_phrase2))

print(re.findall(r'\d+\s+\w+.+', test_phrase2))
print(re.findall(r'\d+\s+\w+.+\.', test_phrase2))
print(re.findall(r'(\d+)(\s+)(\w+)(.+)(\.)', test_phrase2))
print(re.findall(r'(\d+)(\s+)(\w+)(.+?)(\.)', test_phrase2))

multiline_text = """
First line 1
    Second line 3
Third line_ 21
"""
print(re.findall(r'\d+\s+', multiline_text))

print(re.findall(r'\w+\s+\d*', multiline_text))
print(re.findall(r'\w+\s+\d?', multiline_text))
print(re.findall(r'\w+\s+\d{0,1}(?#Like ?)', multiline_text))
print(re.findall(r'\w+\s+\d{1,}(?#Like +)', multiline_text))
print(re.findall(r'\w+\s+\d{0,}(?#Like *)', multiline_text))
print(re.findall(r'\w+\s+\d{0}(?#Not number)', multiline_text))


telephone = 'asd+380969235946'
print(re.search(r'\A\+\d{12}', telephone))


username = "!!!!ОдинОдин!!!user."
print(re.match(r'^[\w.]+$', username))


print(re.findall(r'\A\w+', multiline_text))
print(re.findall(r'^\w+', multiline_text))
print(re.findall(r'\A\w+', multiline_text, re.M))
print(re.findall(r'^\w+', multiline_text, re.M))


telephones = ['+38(096)9235946', '(096)9235946', '+38(096)92-35-946', '+38(096) 923-59-46', '(096) 923-59-46', '+380969235946']
for telephone in telephones:
    match = re.search(r'(?P<country>\+38)?\(?(?P<code>0\d{2})\)?\s*(?P<phone>(?:\d{2,3}-?){3})$', telephone)
    print(match[0])
    print(match.groups())
    print(match.group())
    print(match.start())
    print(match.end())
    print(f"Country: { match.group('country')}, code: { match.group('code')}, phone { match.group('phone')}")


print(re.findall(r'\d+(?=-(?!-))', '138--23-45'))